public class ClassTriangle{
    private String name;
    private double A;
    private double B;
    private double C;
    public ClassTriangle(String name, double A , double B, double C){
        this.name = name;
        this.A = A;
        this.B = B;
        this.C = C;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public double AreaTriangle() {
        double s = (A + B + C) / 2;
        s = s * (s - A) * (s - B) * (s - C);
        s = Math.sqrt(s);
        return s;
    }
    public double PerTriangle() {
        return A + B + C;
    }
    public void printArea(){
        System.out.println(name + ": Area  " + AreaTriangle());
    }
    public void printPer(){
        System.out.println(name + " : Perimeter  " + PerTriangle());
    }
}
