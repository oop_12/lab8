public class ClassCircle {
    private String name;
    private double radian;
    public ClassCircle(String name, double radian){
        this.name = name;
        this.radian = radian;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(String name){
        return name;
    }
    public double areaCircle(){
        double area = Math.PI * (radian*radian);
        return area;
    }
    public double percircle(){
        double per = (2*Math.PI*radian);
        return per;
    }
    public void printArea(){
        System.out.println(name+" :Area "+areaCircle());
    }   
    public void printPer(){
        System.out.println(name+" :Perimeter "+percircle());
        }
        

    }
    
