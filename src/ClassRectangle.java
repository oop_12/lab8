public class ClassRectangle {
    private String name ;
    private double width ;
    private double heigh  ;
    public ClassRectangle(String name ,double width, double heigh){
        this.name = name;
        this.width = width;
        this.heigh = heigh;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name ;
    }
    public double AreaRectangle(){
        double area = width * heigh;
        return area;
    }
    public double Perimeter(){
        return (width + heigh)*2;
    }
    public void printArea(){
        System.out.println(name + " :Area " +AreaRectangle());
    }
    
    public void printPer(){
        System.out.println(name + " :Perimeter " +Perimeter());
    }
    
}

