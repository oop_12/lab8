public class ClassTree {
    private String name;
    private int x;
    private int y;
    public ClassTree(String name,int x,int y){
        this.name = name;
        this.x=x;
        this.y=y;

    }
    public void setName(String name){
        this.name=name;
    }
    //getter setter method
    public String getName(){
        return name;
    }
    public void print(){
        System.out.println(name+" "+ "X:"+x+" "+ "Y:"+y);
    }
    
}
