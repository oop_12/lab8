public class ClassMap {
    private String name;
    private int width;
    private int height;

    public ClassMap(String name,int width,int height){
        this.name=name;
        this.width=width;
        this.height=height;
    }
    public void setName(String name){
        this.name=name;
    }
    //getter setter method
    public String getName(){
        return name;
    }
    public int getWidth(){
        return width;
    }
    public int getHeight(){
        return height;
    }
    public void printMap(){
        for(int x=1;x<=height;x++){
            for(int y =1;y<=width;y++){
                System.out.print("-");
            }
            System.out.println();
        }
    }

    
}
